clean:
	rm -rf target
	rm -rf lib
	mvn clean

jar:
	mvn install dependency:copy-dependencies

run:
	java -jar target/App.jar

go:
	$(MAKE) jar
	echo "\n\n\n"
	$(MAKE) run
